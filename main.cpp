#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <cstdio>
#include <string>
#include <set>
#include <queue>
#include <algorithm>
#include <time.h>

using namespace std;

struct Point
{
	double x, y;
	Point(){}
	Point(int a, int b)
	{
		x = a;
		y = b;
	}
};


double getSize(Point a, Point b)
{
	return sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y));
}

class ImgWork {
private:
	int width, height;
	vector <vector <bool> > visited;
	vector <vector <char> > image;
	vector <string> masks;
	vector <Point> angles;
	vector <Point> figure;
	Point topleft;
	Point botright;
	Point center;

public:

	ImgWork(char *imgFile)
	{
		FILE *img = fopen(imgFile, "rb");
		unsigned char header[54];
		fread(header, 1, 54, img);
		width = header[18];
		height = header[22];
		botright.x = 0;
		botright.y = 0;
		topleft.x = width + 10;
		topleft.y = height + 10;
		visited.assign(height + 5, vector <bool>(width + 5, true));
		for (int i = 3; i < height + 3; i++)
		{
			for (int j = 3; j < width + 3; j++)
			{
				visited[i][j] = false;
			}
		}
		image.assign(height + 5, vector <char>(width + 5, '0'));
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				int r, g, b;
				b = getc(img);
				g = getc(img);
				r = getc(img);
				if (r == 0 && g == 0 && b == 0)
				{
					image[height + 3 - i - 1][j + 3] = '1';
					Point tmp(height + 3 - i - 1, j + 3);
					figure.push_back(tmp);
					if (j + 3 > botright.x) botright.x = j + 3;
					if (height + 3 - i - 1 > botright.y) botright.y = height + 3 - i - 1;
					if (j + 3 < topleft.x) topleft.x = j + 3;
					if (height + 3 - i - 1 < topleft.y) topleft.y = height + 3 - i - 1;
				}
			}
		}
		center.x = (topleft.x + botright.x) / 2;
		center.y = (topleft.y + botright.y) / 2;
		fillMasks();
		for (int i = topleft.y; i <= botright.y; i++)
		{
			for (int j = topleft.x; j <= botright.x; j++)
			{
				string tmp = "";
				tmp.push_back(image[i - 1][j - 1]);
				tmp.push_back(image[i - 1][j]);
				tmp.push_back(image[i - 1][j + 1]);
				tmp.push_back(image[i][j - 1]);
				tmp.push_back(image[i][j]);
				tmp.push_back(image[i][j + 1]);
				tmp.push_back(image[i + 1][j - 1]);
				tmp.push_back(image[i + 1][j]);
				tmp.push_back(image[i + 1][j + 1]);
				for (int k = 0; k < masks.size(); k++)
				{
					if (tmp == masks[k])
					{
						Point bom(i, j);
						angles.push_back(bom);
						break;
					}
				}
			}
		}
	}
	
	void imgPrint()
	{
		for (int i = 3; i < height + 3; i++)
		{
			for (int j = 3; j < width + 3; j++)
			{
				printf("%c", image[i][j]);
			}
			printf("\n");
		}
	}

	void fillMasks() //���������� ������� ����� �����, ���������� ���� ������ �������� ����� �� 90 ��������
	{
		masks.push_back("001011000"); //������
		masks.push_back("010011000"); //������
		masks.push_back("011010000"); //������
		masks.push_back("001010001"); //�����
		masks.push_back("010010101"); //������
		int n = masks.size();
		for (int i = 0; i < n; i++)
		{
			string curMask = masks[i];
			for (int j = 0; j < 3; j++)
			{
				char tmp = curMask[1];
				curMask[1] = curMask[5];
				curMask[5] = curMask[7];
				curMask[7] = curMask[3];
				curMask[3] = tmp;
				tmp = curMask[0];
				curMask[0] = curMask[2];
				curMask[2] = curMask[8];
				curMask[8] = curMask[6];
				curMask[6] = tmp;
				masks.push_back(curMask);
			}
		}
	}

	int angleCounts()
	{
		return angles.size();
	}

	void bfs(int i, int j) //����� �� ����� ��������
	{
		vector <pair <int, int> > shifts;
		shifts.push_back(make_pair(0, 1));
		shifts.push_back(make_pair(0, -1));
		shifts.push_back(make_pair(1, 0));
		shifts.push_back(make_pair(-1, 0));
		queue < Point > q;
		q.push(Point(i, j));
		visited[i][j] = true;
		while (!q.empty())
		{
			Point tmp = q.front();
			q.pop();
			for (int k = 0; k < 4; k++)
			{
				int newI = tmp.x + shifts[k].first;
				int newJ = tmp.y + shifts[k].second;
				if (newI >= 0 && newI < height && newJ >= 0 && newJ < width
					&& image[newI][newJ] == '0' && !visited[newI][newJ])
				{
					q.push(Point(newI, newJ));
					visited[newI][newJ] = true;
				}
			}
		}
	}

	bool isLineType() //���� ��� ���������� ������ ������ ����, �� � ��� ��������� ������
	{
		int k = 0;
		for (int i = 0; i < height; i++)
		{
			for (int j = 0; j < width; j++)
			{
				if (!visited[i][j] && image[i][j] == '0')
				{
					bfs(i, j);
					k++;
				}
			}
		}
		return k == 1;
	}

	double cosTriangle(double a, double b, double c)
	{
		return (a*a + b*b - c*c) / (2 * a * b);
	}

	bool checkIso(double eps1, double eps2, double eps3)  //�������� �� ����������������
	{
		double x = abs(eps1 - eps2);
		double y = abs(eps1 - eps3);
		double z = abs(eps2 - eps3);
		return (x < 0.0001 && y > 0.0001 && z > 0.0001  
			|| y < 0.0001 && x > 0.0001 && z > 0.0001
			|| z < 0.0001 && y > 0.0001 && x > 0.0001);
	}

	bool checkReg(double l1, double l2, double l3)  //�������� �� ������������
	{
		double x = abs(l1 - l2) / l3;
		double y = abs(l1 - l3) / l2;
		double z = abs(l2 - l3) / l1;
		if (x > z) swap(x, z);
		if (y > z) swap(y, z);
		return (z < 0.1);
	}

	void checkTriangle()
	{
		bool is90plus = false, is90 = false, isRegular = false, isIsosceles = false;
		if (angles.size() == 3)
		{
			Point a = angles[0];
			Point b = angles[1];
			Point c = angles[2];
			double l1 = getSize(a, b);
			double l2 = getSize(c, b);
			double l3 = getSize(a, c);
			double cosa = cosTriangle(l1, l2, l3);
			double cosb = cosTriangle(l1, l3, l2);
			double cosc = cosTriangle(l2, l3, l1);
			isIsosceles = checkIso(cosa, cosb, cosc);
			isRegular = checkReg(l1, l2, l3);
			if (cosa >= -0.01 && cosa < 0.01 || cosb >= -0.01 && cosb < 0.01 || cosc >= -0.01 && cosc < 0.01) is90 = true;
			if (cosa < -0.01 || cosb < -0.01 || cosc < -0.01) is90plus = true;
			if (is90plus) cout << " which is obtuse";
			if (is90) cout << " which has 90 degrees angle";
			if (!isRegular && isIsosceles) cout << " which is isosceles";
			if (isRegular) cout << " which is regular";
			cout << endl;
		}
	}

	// ����� 5 ��������� ����� ������ � ��������� ��������� ���������� ���������� 
	// �� ������ ���������� � ��������� eps
	bool checkCircle() 
	{
		bool borderSquare = abs(topleft.x - botright.x) == abs(topleft.y - botright.y);
		if (borderSquare)
		{
			int num = figure.size();
			vector <double> rez;
			double eps = 0.0;
			for (int i = 0; i < 5; i++)
			{
				int tmp = rand() % num;
				double l = getSize(center, figure[tmp]);
				rez.push_back(l);
			}
			for (int i = 0; i < 4; i++)
			{
				eps = max(eps, abs(rez[i] - rez[i + 1]) / rez[i]);
			}
			return (eps < 0.15);
		}
		return false;
	}

	//��������� ��������� 4 ���������� ���������� ����� ���������� �����
	//��������� ���������� ����� ���������������� (4 �������, 2 ���������)
	bool checkSquare()
	{
		bool borderSquare = abs(topleft.x - botright.x) == abs(topleft.y - botright.y);
		vector <double> boms;
		double eps = 0;
		if (borderSquare)
		{
			int num = angles.size();
			for (int i = 0; i < num; i++)
				for (int j = i + 1; j < num; j++)
					boms.push_back(getSize(angles[i], angles[j]));
			sort(boms.begin(), boms.end());
			for (int i = 0; i < 3; i++)
				eps += abs(boms[i] - boms[i + 1]);
			return (eps < 0.1);
		}
		return false;
	}
	
	//�������� �������
	void figureType()
	{
		if (isLineType())
		{
			if (angleCounts() == 0) cout << "A line" << endl;
			else cout << "Angled line, number of angles: " << angleCounts() << endl;
		}
		else
		{
			if (angleCounts() == 0 || angleCounts() > 4)
			{
				if (checkCircle()) cout << "A circle" << endl;
				else cout << "An ellipse" << endl;
			}
			else if (angleCounts() == 3)
			{
				cout << "A triangle";
				checkTriangle();
			}
			else if (angleCounts() == 4)
			{
				if (checkSquare()) cout << "A square" << endl;
				else cout << "A rectangle";
			}
		}
	}
};

int main()
{
	srand(time(NULL));
	ImgWork test("testTriangle.bmp");
	test.figureType();
	return 0;
}